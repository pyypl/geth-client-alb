# geth-client-alb

Ethereum client that waits for synchronization to complete before registering
with AWS Application Load Balancer.

Deregisters automatically if client goes out of sync, or is shutting down.

Based on ethereum/go-client.

## configuration

Following environment variables can be configured:

| variable         | required | default   | description    |
| ---------------- | -------- | --------- | -------------- |
| AWS_REGION       |    x     |           |                |
| TARGET_GROUP_ARN |    x     |           |                |
| POLL_INTERVAL    |          | 10000     | Interval in ms |
| GETH_ADDRESS     |          | localhost |                |
| GETH_PORT        |          | 8545      |                |
| LOG_LEVEL        |          | info      |                |
| MINIMUM_PEERS    |          | 3         |                |
